# Kubernetes


## Copy & Paste

```sh
# Delete all pods
kubectl get pods | awk 'NR>1 {print $1}' | xargs -L1 kubectl delete pod

# Get Kubernetes master, KubeNDS und Dashboard information
kubectl cluster-info

# get all ressources
kubectl get all --all-namespaces

# resource monitor
kubectl top nodes

# Create Secrets
kubectl create secret generic db-zombie-pass --from-file=./username.txt --from-file=./password.txt

# Exchange value from secret
cat secrets/nginx.pem | base64

# Run interactive pod
kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il

 kubectl run mycontainer -i --tty --image=itsthenetwork/alpine-tcpdump:latest --restart=Never --command -- /bin/ash

# Run debugging pod
kubectl run -i  --tty --image=<my-image> --command -- sh

# Forward traffic of specific port 
kubectl port-forward <mypod> 8080:80

# Create List of endpoints
kubectl get services -l app=sparktraining --all-namespaces | awk 'FNR > 1 {print $4}' | xargs -L1 -I '$' echo '$:8080'

# Wait till loadbalancers are ready / pubic IPs assigned
while [[ $(! kubectl get services -l tool=zeppelin --all-namespaces | grep "<pending>") ]]; do sleep 10; done
```

## Manifest

```yaml
# dummy example manifest
apiVersion: v1
kind: Pod
metadata:
  name: dummy
  labels:
    app: "dummy"
spec:
  volumes:
  - name: shared-data
    emptyDir: {}
  containers:
  - name: nginx-container
    image: alpine
    volumeMounts:
    - name: shared-data
      mountPath: /data
    command: ["/bin/sh"]
    args: ["-c", "while sleep 3600; do :; done"] 
```


### Mount configs

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-configmap
  namespace: ...
data:
  foo.yaml: |-
    blacklist: ['val1', 'val2']
  test.sh: |-
    echo Test at `date`
```


```yaml
kind: Deployment
spec:
  replicas: 1
  template:
    spec:
      volumes:
      - name: my-configs
        configMap:
          name: my-configmap
      containers:
      - name: ...
        image: ...
        volumeMounts:
		  - name: my-configs
			 mountPath: /etc/curator
			 subPath: fo.yaml
```

### Self signed certificate
```sh
# create certificate
openssl req -nodes -newkey rsa:4096 -keyout registry-auth.key -out registry-auth.csr -subj "/CN=registry.example.com"

# sign key
openssl x509 -in registry-auth.csr -out registry-auth.crt -req -signkey registry-auth.key -days 3650

# Create secret
kubectl create secret tls my-tls-secret --cert=registry-auth.crt --key=registry-auth.key

certificate signing request
```

## Helpers

### Output Formatting

```sh
# JsonPath
kubectl get pods -n namespace -o jsonpath='{.items[0].spec.containers[*].ports[*].containerPort}'
kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services <name>

# Get all Pod IPS
kubectl get pods -o jsonpath='{.items[*].status.podIP}'
kubectl get pods -o json | jq '.items[].status.podIP'

kubectl get service -o=custom-columns="NAME:.metadata.name,IP:.spec.clusterIP,PO‌​RT:.spec.ports[*].ta‌​rgetPort"
# select entry
kubectl get nodes -o custom-columns='ab:{.status.addresses[?(.type=="ExternalIP")].address}'

kubectl get nodes -o=custom-columns="NAME:.metadata.name,IP:.status.addresses[*].address,PO‌​RT:.spec.ports[*].ta‌​rgetPort"
```


```sh
# Custom Columns
kubectl get service -n hadoopsummit -o=custom-columns=NAME:metadata.name,IP:spec.clusterIP,PORT:{spec.. containerPort}
```


## Using REST-API

```sh
# Access API using kubectl proxy
curl -X GET localhost:8001/api/v1/pods?labelSelector=app=hivemq | jq '.items[].status.podIP'

# Access API from within a pod
curl -s --cacert /var/run/secrets/kubernetes.io/serviceaccount/ca.crt -H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" https://kubernetes/api/v1/pods?labelSelector=app=hivemq | jq '.items[].status.podIP'
```


### Access api within a container 

1. [Configure Service Account](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/)
	
	 * Create ServiceAccount
	 	
	 ```sh yaml
		apiVersion: v1
		kind: ServiceAccount
		metadata:
			name: build-robot
	 ```
	 
	 `kubectl get serviceaccounts -o yaml`
	 
	 * Create Secret for ServiceAccount

	 ```sh yaml
		apiVersion: v1
		kind: Secret
		metadata:
		  name: build-robot-secret
		  annotations: 
		    kubernetes.io/service-account.name: build-robot
		type: kubernetes.io/service-account-token
	 ```
	 
	 `kubectl get secret -o yaml`
	 
	 * [Specify ImagePullSecrets] (https://kubernetes.io/docs/concepts/containers/images/# specifying-imagepullsecrets-on-a-pod)
	
	 ```sh
	kubectl create secret docker-registry myregistrykey --docker-server=DOCKER_REGISTRY_SERVER --docker-username=DOCKER_USER --docker-password=DOCKER_PASSWORD --docker-email=DOCKER_EMAIL
	```
	
	 * Add Image pull policy
	 
	 	```sh
	 	kubectl get serviceaccounts default -o json |
	     jq  'del(.metadata.resourceVersion)'|
	     jq 'setpath(["imagePullSecrets"];[{"name":"myregistrykey"}])' |
	     kubectl replace serviceaccount default -f -
	 	```
 
 
### Replace env variables in config

```
envsubst < ./proxy.config.json > ./proxy.config.json
```

### [YAML multi-line-strings](https://stackoverflow.com/questions/3790454/in-yaml-how-do-i-break-a-string-over-multiple-lines/21699210)


``` sh
> # Folded style removes the newlines within the string
| # Literal style turns newlines within the string into literal newlines
>, |: # "clip": keep line feed, remove trailing blank lines.
>-, |-: # "strip": remove line feed, remove trailing blank lines.
>+, |+: # "keep": keep the line feed, keep trailing blank lines.
```

**Example 1**

```yaml
Key: >
  foo 
  bar
# → foo bar\n

Key: |
  foo
  bar
# → foo\nbar\n
```

**Example 2**

```sh
- <OPERATOR>
  very "long"
  'string' with

  paragraph gap, \n and        
  spaces.
  

# OPERATOR >
"very \"long\" 'string' with\nparagraph gap, \\n and         spaces.\n"
# OPERATOR |
"very \"long\"\n'string' with\n\nparagraph gap, \\n and        \nspaces.\n"
# None
"very \"long\" 'string' with\nparagraph gap, \\n and spaces."
# Encapsulated "
"very \"long\" 'string' with\nparagraph gap, \n and spaces." 
# Encapsulated '
"very \"long\" 'string' with\nparagraph gap, \\n and spaces." 
# OPERATOR >- 
"very \"long\" 'string' with\nparagraph gap, \\n and         spaces."
```


### Copy-Paste

```yaml
# Let container sleep forever
command: [ "/bin/bash", "-c", "--" ]
args: [ "while true; do sleep 30; done;" ]
```

### Enable LoadBalancing without AWS/GCE
[Use Service Loadbalancer](https://github.com/kubernetes/contrib/tree/master/service-loadbalancer)

## Helm

```sh
# Output kubectl files
helm install --dry-run --debug

# Split output into multiple files
brew install coreutils
gcsplit output/export '/\-\-\-/' '{*}'
for f in ./xx*; do export ab=./target/$(cat $f | sed -n -e 's/^.*Source: //p');  echo $ab; mkdir -p ${ab%/*} && mv $f "$ab"; done;
```

```sh
# overwrite values inline
helm upgrade zep helm-zeppelin --set resources.requests.cpu=50m
```

## Helpers

```sh
# wait till pod is ready
curl -v --cacert /var/run/secrets/kubernetes.io/serviceaccount/ca.crt -H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" https://kubernetes.default

# Get pods within namespace from pod using Web-API
KUBE_TOKEN=$(</var/run/secrets/kubernetes.io/serviceaccount/token)
NAMESPACE=$(</var/run/secrets/kubernetes.io/serviceaccount/namespace)
curl -sSk -H "Authorization: Bearer $KUBE_TOKEN" \
      https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_PORT_443_TCP_PORT/api/v1/namespaces/$NAMESPACE/pods/$HOSTNAME


# wait till file exists
while [ ! -f /home/gitlab/data/backups/*_2017_09_19_* ]; do sleep 60; done
```


## Kubernetes-Networking:

### Service Types

* __ClusterIP (default)__: cluster-internal IP, makes the service only reachable within the cluster
* __NodePort__: Exposes service on each Node’s IP at a static port (the NodePort). ClusterIP service is auto created. Access from outside via <NodeIP>:<NodePort>.
* __LoadBalancer__: expose externally using cloud provider’s load balancer. NodePort and ClusterIP services are automatically created.
* __ExternalName__: Map service to externalName field (e.g. foo.bar.example.com), by returning a CNAME record with its value. No proxying of any kind is set up. This requires version 1.7 or higher of kube-dns.

### sshuttle