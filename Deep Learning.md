# Deep Learning

[Udemy Course](https://www.udemy.com/deeplearning) [Course Folder Template](http://superdatascience.com/deep-learning)

## General

Activation Functions $y$ in relation to $ \sum^m_{i=1}{w_ix_i} $$:$

 * Threshold function: 1 if x ≥ 0, 0 if x <0
 * Sigmoid Function $$ \frac{1}{1+e^{-x}} $$
 * Rectifier (ReLu) $$ max(x,0) $$
 * Hyperbolic Tangent (tanh) $$ \frac{1-e^{-2x}}{1+e^{-2x}} $$

Problems of Sigmoid vs ReLu:

* Vanishing Gradients: The bigger the input, the smaller the gradient of the sigmoid function. Especially it is allways smaller then one, mostly ~0,25 => Many layers quickly decreases to 0!
* ReLu blows up activation 


<img src="img/Screen Shot 2017-12-29 at 13.13.00.png" alt="tanh" width="200px" height="120px">
<img src="img/Screen Shot 2017-12-29 at 13.13.56.png" alt="sigmoid" width="200px" height="120px">
 
 
 Cost Function $$\frac{1}{2}(\hat{y}-y)^2$$
 
*  Batch Gradient Descent: Deterministic, 
* Stochastic Gradient Descent: Faster, Higher Chance not to stuck in local minimum
* Mini-Batch Gradient Descent: 
 
 
### Cross-Validation
 ```python
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from keras.models import Sequential
from keras.layers import Dense
def build_classifier(optimizer):
    classifier = Sequential()
    classifier.add(Dense(units = 6, kernel_initializer = 'uniform', activation = 'relu', input_dim = 11))
    classifier.add(Dense(units = 6, kernel_initializer = 'uniform', activation = 'relu'))
    classifier.add(Dense(units = 1, kernel_initializer = 'uniform', activation = 'sigmoid'))
    classifier.compile(optimizer = optimizer, loss = 'binary_crossentropy', metrics = ['accuracy'])
    return classifier
classifier = KerasClassifier(build_fn = build_classifier('adam'), batch_size = 10, epochs = 100)
accuracies = cross_val_score(estimator = classifier, X = X_train, y = y_train, cv = 10, n_jobs = -1)
mean = accuracies.mean()
variance = accuracies.std()
 ```

### Dropout

```python
from keras.layers import Dropout
classifier.add(Dropout(p = 0.1))

```


### Grid Search

```python
from sklearn.model_selection import GridSearchCV
classifier = KerasClassifier(build_fn = build_classifier)
parameters = {'batch_size': [25, 32],
              'epochs': [100, 500],
              'optimizer': ['adam', 'rmsprop']}
grid_search = GridSearchCV(estimator = classifier,
                           param_grid = parameters,
                           scoring = 'accuracy',
                           cv = 10)
grid_search = grid_search.fit(X_train, y_train)
best_parameters = grid_search.best_params_
best_accuracy = grid_search.best_score_
``` 

## CNN

Convolution (Apply "Feature Detector" (= Kernel) on window) => Detect special features
Pooling: (Preserve Features not carring about where it exactly is)
Flatten:  

SoftMax:
Cross-Entropy: $$\frac{1}{2}(\hat{y}-y)^2$$


```python 
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense

classifier = Sequential()
classifier.add(Conv2D(32, (3, 3), input_shape = (64, 64, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))
# Adding more convulational layers
classifier.add(Flatten())
classifier.add(Dense(units = 128, activation = 'relu'))
classifier.add(Dense(units = 1, activation = 'sigmoid'))

# Compiling the CNN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Part 2 - Fitting the CNN to the images

from keras.preprocessing.image import ImageDataGenerator

train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('dataset/training_set',
                                                 target_size = (64, 64),
                                                 batch_size = 32,
                                                 class_mode = 'binary')

test_set = test_datagen.flow_from_directory('dataset/test_set',
                                            target_size = (64, 64),
                                            batch_size = 32,
                                            class_mode = 'binary')

classifier.fit_generator(training_set,
                         steps_per_epoch = 8000,
                         epochs = 25,
                         validation_data = test_set,
                         validation_steps = 2000)

```

## RNN


## Self Organizing Maps


## Boltzmann Machines


## Auto Encoders
 
 