# GCloud


## Setup
```sh 
# update cli / kubectl
gcloud components update

# Setup account
gcloud init 

# set compute zone
gcloud config set compute/zone europe-west1-b

# switch project
gcloud config set project <project_name>

# Set kubectl
gcloud container clusters get-credentials <clusterName>
```

## Compute

``` sh
# Loginto instance
gcloud compute ssh gitlabcirunnernew --zone us-west1-b 

# Copy files
gcloud compute copy-files ~/LOCAL-FILE-1 ~/LOCAL-FILE-2 example-instance:~/REMOTE-DIR --zone us-central1-a 
```


## Container

```sh
# Extend Cluster withnodes
gcloud beta container node-pools create <nameOfNodepool> --cluster <myCluster> -m n1-standard-2 --zone europe-west1-b --preemptible
```

## Storage (s3 compability)

```sh
# Create bucket and upload files
gsutil mb gs://my-awesome-bucket/
gsutil cp *.txt gs://my-bucket

# Sync folders / upload + download files
gsutil rsync -d -r data gs://mybucket/data

# Enable read permission to everyone (set default to public)
gsutil defacl ch -u allUsers:R gs://<bucket>

# Enable write permission to everyone
gsutil acl ch -u allUsers:O gs://<bucket>
```

__Create signed URL__
[Service Account](https://console.cloud.google.com/apis/credentials?project=<project>&organizationId=<organizationId>)

### Cleanup

```sh
# Remove left-overs from k8s clusters
gcloud compute forwarding-rules list | awk 'NR>1 {print $1}' | paste -sd " " - | xargs -L1 gcloud compute forwarding-rules delete --quiet --region europe-west1
gcloud compute disks list | grep gke- | awk '{print $1}' | paste -sd " " - | xargs -L1 gcloud compute disks delete -q
gcloud compute target-pools list | awk '{print $1}' | tail -n +2  | paste -sd " " -
gcloud compute firewall-rules list | awk '{print $1}' | grep k8s | xargs -L1  gcloud compute firewall-rules delete --quiet

```