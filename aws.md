# AWS

[Activate Code Completion](http://docs.aws.amazon.com/cli/latest/userguide/cli-command-completion.html): `source /usr/local/bin/aws_zsh_completer.sh`

```sh
aws configure set default.region eu-central-1 # Frankfurt / eu-west-1 (Dublin)
```

```sh
# Set AWS profile
export AWS_DEFAULT_PROFILE=default
aws --profile <profile> <my command>
```

## Networking

Region: 1:n AZ n:m VPC 1:n Subnets

Beispiel: 

## EC2

Most important AMIs:

AMI | Description
--- | ---
ami-b03ffedfm |Ubuntu Server 16.04 LTS (HVM), SSD Volume Type
ami-e80dc287

[Instance-Types](https://aws.amazon.com/ec2/instance-types/): 

* t2: General purpose: Baseline level of CPU with ability to burst above (webservers etc.)
* M4/M3: latest Generation general Purpose: 2.3/2.4 GHz Intel Xeon, SSD (databases)
* C4/C3: Compute optimized
* X1/R4/R3: Memory optimized


Modell | vCPU  | Credits/h | RAM (GB)
--- | --- | --- | ---
t2.nano | 1 | 3 | 0,5
t2.micro | 1 | 6 | 1  
t2.small | 1 | 12 | 2  
t2.medium | 2 | 24 | 4  
t2.large | 2 | 36 | 8  
t2.xlarge | 4 | 54 | 16 
t2.2xlarge | 8 | 81 | 32 


```sh
# start instance
aws ec2 run-instances --image-id ami-xxxxxxxx --count 1 --instance-type t1.micro --key-name MyKeyPair --security-groups my-sg

# Describe instances
aws ec2 describe-instances | grep 'InstanceId\|Tags'
aws ec2 describe-instances | jq -c '.Reservations[].Instances[] | select(.Tags[].Value | contains( "Gitlab Runner")) | .InstanceId'

# Filter instances
aws ec2 describe-instances --filters "key=name,Value=Gitlab Runner"
aws ec2 describe-instances --instance-ids i-g93g494d i-a93f754c

# Show instances in table format (summary)
aws ec2 describe-instances --query "Reservations[*].Instances[*].{name: Tags[?Key=='Name'] | [0].Value, instance_id: InstanceId, private: PrivateIpAddress, public: PublicIpAddress, state: State.Name}" --output table

# Get all instances with a specific tag
aws ec2 describe-instances | jq '.Reservations[].Instances[] | select(.Tags[].Value="kz8scluster.christianstest.bmw-fleet.net")'

aws ec2 describe-instances --filters "Name=tag:cluster_name,Values=nico" "Name=tag:k8s.io/role/node,Values=1" --query "Reservations[*].Instances[*].{name: Tags[?Key=='Name'] | [0].Value, instance_id: InstanceId, private: PrivateIpAddress, public: PublicIpAddress, state: State.Name}" --output table

# stop instance
aws ec2 terminate-instances --instance-ids

# SSH login
aws ec2 
```

__IP binding__:

```sh
# Get private IP
aws ec2 allocate-address
aws ec2 describe-addresses

# Assign address to instance
aws ec2 associate-address --instance-id <InstanceId> --allocation-id <PublicIp>
```


## Load-Balancing

* ELB (Classic LB): IP Layer
* ALB (Application LB): HTTP-Layer, 

```sh
# Add Self-signed certificate
openssl rsa -in server.key -text > private.pem
openssl x509 -inform PEM -in server.crt > public.pem
aws iam upload-server-certificate --server-certificate-name blah --certificate-body file://path/to/server.crt --private-key file://path/to/private.key --path /cloudfront/static/
```

## ECR

## S3

```sh
# list buckets
aws s3 ls

# list files within a bucket
aws s3 ls s3://mybucket

# list all files with size in bucket
aws s3 ls --summarize --human-readable --recursive s3://mybucket

# Create bucket
aws s3 mb s3://mybucket

# Sync bucket with local file
s3 sync s3://mybucket/folder ./folder
s3cmd sync s3://mybucket/folder ./folder

# Calculate total storage size
aws s3 ls --summarize --human-readable --recursive s3://mybucket
```

## Networking & Security

* __VPC__: virtual network dedicated to an AWS account and isolated from other VPCs (By default spans over all AZs within one region)
* __subnet__: Part of a vpc, that manages a CIDR block and resides entirely in one AZ
* Security
	* __security group__: Controls inbound & outbound traffic for an instance
	* __network ACLs__: Controls inbound & outbound traffic for a subnet
* NAT:
	* NAT Gateway: Nat as a service (AWS managed)
	* NAT Instance: Dedicated machine, running an AMI to provide NAT functionality

## IAM & Policies

Effect (Required) – specifies whether the statement will explicitly allow (“Allow”) or deny (“Deny”) access. These are the only two values that are valid in this element.

Action* (Required) – describes the type of access that should be allowed or denied.

Resource* (Required) – specifies the object or objects that the statement covers.

Principal* (Optional) – specifies the user, account, service, or other entity that is allowed or denied access to a resource. Principals can only be used for resource-based policies. For policies within IAM, the policy is attached to the Principal it applies to.

Condition (Optional) – lets you specify conditions for when a policy is in effect.