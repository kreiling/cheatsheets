# PostgreSQL

## Connect

```sh
# find existing connection information in ~/.pgpass
psql --username=user --host=server --port=5432 DB

# List Databases
SELECT datname FROM pg_database
WHERE datistemplate = false;

# List tables in the current database
SELECT table_schema,table_name FROM information_schema.tables ORDER BY table_schema,table_name;
```