# RDBMS

## Postgres

```sh
sql -U <user> -h <Host> -d schema -f test.sql
```

### sql
```sql
# List all schema names
select schema_name from information_schema.schemata;

# List all tables of schema
SELECT * FROM pg_catalog.pg_tables where schemaname="s71";

# Describe table 
```

### psql
```sh
\?	# Postgres Help function

\d[S+] # List 
\d[S+] NAME # describe table, view, sequence, or index

\l 			# list databases
\dn			# List schemas
set search_path to myschema;
\dt[S+]     # list tables
\d+ mytable # Describe table

\ds[S+]     # list sequences
\dt[S+]     # list tablesse
\dT[S+]     # list data types
\du[+]      # list roles
\dv[S+]     # list views
\dE[S+]     # list foreign tables
\dx[+]      # list extensions
\dy         # list event triggers
```
