# Kafka

```sh
# Get topics
/usr/bin/kafka-topics --zookeeper zk01.example.com:2181 --list

# Create topic
bin/kafka-topics.sh --create --zookeeper zookeeper.kubigdata --replication-factor 1 --partitions 1 --topic test

bin/kafka-console-producer.sh --broker-list kafka.kubeyard:9092  --topic test

bin/kafka-console-consumer.sh --bootstrap-server kafka.kubeyard:9092  --topic test --from-beginning

```
