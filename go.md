# Go

## Hello World

### Install dependencies

```sh
go get github.com/jehiah/json2csv
trash -u && trash
```

``` sh
go get -d ./...
```

### Build / run

``` sh
# Interpret package
go run hello-world.go

# Build package
go build hello-world.go
```

### Blanc hello-world template

```go
package main
import "fmt"
func main() {
    fmt.Println("hello world")
}
```


## Basics

### Built-in Types

* bool
* string
* int  int8  int16  int32  int64
* uint uint8 uint16 uint32 uint64 uintptr
* byte // alias for uint8
* rune // alias for int32 ~= a character (Unicode code point) - very Viking
* float32 float64
* complex64 complex128

### Types, Functions, Interfaces

```go
// some function with return
func f(from string) {
    for i := 0; i < 3; i++ {
        fmt.Println(from, ":", i)
    }
}

// function with multiple returns
func vals() (int, int) {
    return 3, 7
}

// function with named return value
func learnNamedReturns(x, y int) (z int) {
    z = x * y
    return // z is implicit here, because we named it earlier.
}

// variadic function (arbitary number of arguments)
func sum(nums ...int) {
    fmt.Print(nums, " ")
    total := 0
    for _, num := range nums {
        total += num
    }
    fmt.Println(total)
}

// Define your own type
type rect struct {
    width, height int
}

// Add method to type (the method has a receiver type of *rect.)
func (r *rect) area() int {
    return r.width * r.height
}

// Interface 
type geometry interface {
    area() float64
}

// general function for all objects implementing geometry interface
func measure(g geometry) {
    fmt.Println(g)
    fmt.Println(g.area())
}
```

### Loops, Conditionals and Errors

``` go
func f1(arg int) (int, error) {
    if arg == 42 {
        return -1, errors.New("can't work with 42")
    }
    return arg + 3, nil
}

for _, i := range []int{7, 42} {
    if r, e := f1(i); e != nil {
        fmt.Println("f1 failed:", e) 
    } else {
        fmt.Println("f1 worked:", r)
    }
}
```


### Array, ranges, maps

``` go
// Arrays (fixed length)
var arr [3]int // Create blank [0,0,0]
arr2 := [5]int{1, 2, 3, 4, 5} // Create with default values

// Slices (variable length)
s := make([]string, 3) // create
s[0] = "a" // set value
fmt.Println(s[0]) // get value
s = append(s, "d") // append value

// Maps ( associative array => (key,value)-pairs )
m := make(map[string]int) // create
m["k1"] = 7 // set value
fmt.Println(m["k1"]) // get value

// Range (iterates over elments in different data structures
for i, num := range arr2 {
    if num == 3 {
        fmt.Println("index:", i)
    }
}
    
for k, v := range kvs {
    fmt.Printf("%s -> %s\n", k, v)
}
```

### String functions

```go
p := point{1, 2} 
fmt.Printf("%v\n", p) // prints an instance of our point struct: {1 2}
fmt.Printf("%+v\n", p) //struct’s field names: {x:1 y:2}
fmt.Printf("%# v\n", p) // Go syntax representation of the value: main.point{x:1, y:2}
fmt.Printf("%T\n", p) // type of a value: main.point
```

```go
import s "strings"
s.Contains("test", "es") 	// Contains: true
s.Count("test", "t") 		// Count: 2
s.HasPrefix("test", "te") 	// HasPrefix: true
s.HasSuffix("test", "st") 	// HasSuffix: true
s.Index("test", "e") 		// Index: 1
s.Join([]string{"a", "b"}, "-")	// Join: a-b
s.Repeat("a", 5) 			// Repeat: aaaaa
s.Replace("foo", "o", "0", -1) 	// Replace: f00
s.Replace("foo", "o", "0", 1) 	// Replace: f0o
s.Split("a-b-c-d-e", "-") 	// Split: a b c d e]
s.ToLower("TEST") 			// ToLower: test
s.ToUpper("test") 			// ToUpper: TEST
```

### Co-Routines (lightweight thread of execution)

```go
// execute thread on existing function
go f("goroutine")

// interactive function thread execution
go func(msg string) {
    fmt.Println(msg)
}("going")

messages := make(chan string)
go func() { messages <- "ping" }()
msg := <-messages
fmt.Println(msg)
```


## Networking

### HTTP-Requests

```go
// Authenticated HTTPS request
import net/http
func basicAuth() string {
    var username string = "foo"
    var passwd string = "bar"
    client := &http.Client{}
    req, err := http.NewRequest("GET", "https://mydomain.com", nil)
    req.SetBasicAuth(username, passwd)
    resp, err := client.Do(req)
    if err != nil{
        log.Fatal(err)
    }
    bodyText, err := ioutil.ReadAll(resp.Body)
    s := string(bodyText)
    return s
}
```



### HTTP-Server

```go
package main

import (
    "fmt"
    "net/http"
    "strings"
    "log"
)

func sayhelloName(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()  // parse arguments, you have to call this by yourself
    fmt.Println(r.Form)  // print form information in server side
    fmt.Println("path", r.URL.Path)
    fmt.Println("scheme", r.URL.Scheme)
    fmt.Println(r.Form["url_long"])
    for k, v := range r.Form {
        fmt.Println("key:", k)
        fmt.Println("val:", strings.Join(v, ""))
    }
    fmt.Fprintf(w, "online!") // send data to client side
}

func main() {
    http.HandleFunc("/", sayhelloName) // set router
    err := http.ListenAndServe(":9090", nil) // set listen port
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}
```