# Shell & Bash

## Working with files

```sh
# Create Array
arr=(one two three)

# For loop
for f in ./bss_ddl/*; do hive -f $f; done;

# For loop in range to download pictures
for f in {6900..9000}; do wget --quiet --no-check-certificate -O ~/Downloads/pics/${f}.jpg https://gallery.de/folders/IMG_${f}.JPG; done;

ls -1 | wc -l # count files in folder
du -sh /* # get file size 

# Get the file modified last in current directory
find . -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" "
```


### `cp` / `mv` files corretly
([Never use src/](http://stackoverflow.com/questions/18158248/should-i-put-trailing-slash-after-source-and-destination-when-copy-folders))

```sh
# basedir (path to script location)
BASEDIR=$(dirname "$0")

cp -r /src /dest # Copy file to src
cp -r /src /dest/ # Move files into dest

mv -t DESTINATION file1 file2 file3 # move multiple files (Linux)
mv file1 file2 file3 DESTINATION # move multiple files (Mac)
```

## Basics

### Burnshell

```sh
# Burnshell (start script)
\# !/bin/sh

set -e # Exit on error

# If-Statement (multi line)
if [ $Name != $USER ]
then
    echo "true"
else; echo "false"; fi

# If-Statement (single line)
if [ $? == 0 ]; then echo "removed backup"; else echo "no backup found"; fi;

# if-Statement compact (check if file has more then 1 line)
if [ $(wc -l < ./file.txt) -gt 1 ]; then echo "AB"; else echo "BC"; fi;
 
if [ ! -f "$FILE" ] # Check if file exists
if [ ! -s "$FILE" ] # Check if is not empty 
if [ -z $cores ]; # Check if variable is set


# sort files by time
find . -name '*.tar'  -printf "%T@ %Tc %p\n" | sort -n
```

### Bash

```sh
# Bash Verbose output and exit on error
\# !/bin/bash -xe

# Read value
read -p "You will receive an e-mail. Click the activation link and continue." -n1 -s

# use script parameter with default value
export target_mail=${3:-my@default.de}

# Read value of variable
export var_name=USER && echo "${!var_name}"

# if-Statement Bash (multi line)
if [[ "$*" == *".prod."* ]]; then
    echo works
else
    echo error
fi

# Custom functions
my-func() {
    echo $2
}

my-func ab cd e.prod.f
```

```sh
# return error code
function killFn(){return 1}

# print return code (of last command)
echo $?

# Exit script based on return code
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# ignore error and keep on running program
killFn || true && echo "test"

# redirect stdout and stderr to 2 different files
command 2>> error 1>> output

# only use stderr
command  2>&1 >/dev/null | grep
```


```sh
# Download zip, unpack and save at var
wget -q -O - https://www.dropbox.com/s/rqj1ndi0qv0mty6/data.zip?dl=0 | tar -xzf - -C /var

# Download and extract single executable file from gz (install helm) 
wget https://storage.googleapis.com/kubernetes-helm/helm-v2.4.1-linux-amd64.tar.gz -qO- | tar -xvzf - linux-amd64/helm -C /usr/local/bin/  --strip-components 1
```

```sh
# Pseudo Terminal / EOF Script
ssh my.server << EOF
    hdfs dfs -du -h hdfs://nn01.prod.de:50070/user/
EOF

# Pseudo Terminal / EOF Script pipe into next function
ssh my.server << | jq '.' EOF
    kubectl get secrets -o json
EOF
```


# Utilities

```sh
# Wordcount only output line number
wc -l < file.txt
```

## Bash
```sh
hist
!<command> # reruns the last command starting with entered the text from bash history
!<command>:p # reruns the last command starting with entered the text from bash history
!! # reruns the last command starting with entered the text from bash history
!<NR> # reruns the last command starting with entered the text from bash history
vi !$
```


```sh
# Access ENV variables
 myCmd=cloudrunner_CI_K8S_PROJECT
  # - export CI_K8S_PROJECT=${!myCmd}
  # - echo $CI_K8S_PROJECT
```