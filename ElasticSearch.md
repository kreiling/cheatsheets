# Elastic Search

## Basic concepts

### Data Related
* __Index__ (Database): Collection of related documents
	* Show all indices `GET /_cat/indices?v`
	* Create index: `PUT /customer?pretty`
	* Stats: `GET /customer/_stats?pretty`
	* Settings: `GET /customer/_settings?pretty&v`
* __Type__ (Table): Consists of a name and a mapping, which describes the fields or properties that documents of that type must have
	* List Mappings: `GET /customer/_mapping?pretty=true` 
	* List Documents in Type: `GET /customer/external/_search`
	* Object per Mapping: `GET /customer/external/_count`
* __Document__ (Row): Basic unit of information, always belongs to one type (dynamic mapping creates type if it does not exist yet)
	* Create Document `PUT /customer/external/1?pretty {"name": "John Doe"}`
	* Query Document: `GET /customer/external/1?pretty`
	* Update Document `POST /customer/external/1/_update?pretty { "doc": { "name": "Jane Doe", "age": 20 }}`
	* Return first 10 Documents `GET /meta/_search/?size=10&pretty`
	* Return fields of Document `GET /customer/external?_source=title,text`
	
		``` js
		{
			"_index" :   "website",
			"_type" :    "blog",
			"_id" :      "123",
			"_version" : 1,
			"found" :   true,
			"_source" : {
				"title": "My first blog entry",
				"text":  "Just trying this out"
			}
		}
```

[Optimize Segements](https://www.elastic.co/guide/en/elasticsearch/reference/1.7/indices-optimize.html)

```sh
curl -XPOST 'http://localhost:9200/kimchy,elasticsearch/_forcemerge?max_num_segments=1'
```

* __buckets__(group by)
* __search__(where): 
 
### Infrastructure Related
* __Cluster__
	* Cluster Health`GET /_cat/health?v`
	* Cluster Stats: `GET _cluster/stats?human&pretty`
* __Node__
	* List of nodes `GET /_cat/nodes?v`
* __Shards__: Divide the full index into multiple smaller peaces to enable horizontal scaling in volume and increase read/write-throughput since operations can run on multiple machines in parallel  ([Docs](https://www.elastic.co/guide/en/elasticsearch/reference/5.1/_basic_concepts.html))
* __Replicas__: Create multiple copies of the index's shards to improve availablability (protect against data loss) and increase search volume throughput since operations can be executed on each replica in parallel


## Access Data

### WebAPI (Browserurl)
 ```sh
<host>/_cat/ # List avaialbe operations
<host>/test/type/_mget # get multiple documents

# Search API
http://localhost:9200/customer/external/_search?pretty&q=_type:exter~ # Pretty-Print search results for documents belonging to a type starting with exter 
<host>/inca/employee/_search?q=firstName:nico # search documents with firstName = nico

# Search using URL parameters (Spaces need to be encoded)
<host>/_search?q=name:john~1AND(age:[30TO40}OR(surname:K*))AND(-city)

<host>/inca,someother/_mapping/employee?pretty # Describe type

 ```
 
### Kommandozeile (curl)

```sh
curl -XDELETE 'localhost:9200/inca?pretty'
```


#### Query-Examples using DSL (Domain Specific Language)
 ```sh

curl -XGET 'localhost:9200/_search?pretty' -d'
{
  "query": { 
    "bool": { 
      "must": [
        { "match": { "title":   "Search"        }}, 
        { "match": { "content": "Elasticsearch" }}  
      ],
      "filter": [ 
        { "term":  { "status": "published" }}, 
        { "range": { "publish_date": { "gte": "2015-01-01" }}} 
      ]
    }  
  }
}'
 
curl -XGET 'localhost:9200/_search?pretty' -d'
{
    "query": {
        "match_all": { "boost" : 1.2 }
    }
}'

 ```
 
 **Filter by date**
 
 ```sh
 # First seen in the last two days
GET index/mapping/_search
{
  "query": {
    "filtered": {
      "filter": {
        "range": {
          "first_seen_at": {
            "gte": "now-2d/d",
            "lt": "now/d"
} } } } } }
 ```


## Index API
#### Template

 ```sh
PUT _template/template_1 
{
  "template": "te*", # Apply to all indices which start with te
  "settings": { # Settings to apply
    "number_of_shards": 1
  },
  "mappings": { # Mappings to apply
	"dynamic": "strict", # Reject object which don't fit in the mapping schema
	"type1": {
      "_source": {
        "enabled": false # Makes the _source field 
      },
      "properties": {
        "host_name": {
          "type": "keyword"
        },
        "created_at": {
          "type": "date",
          "format": "EEE MMM dd HH:mm:ss Z YYYY"
        }
      }
    }
  }
}
 ```


## Search API

#### Boolean Query Operators:

* must: must exist and contributes to score
* filter: must exist but does not contribute to score
* should: contributes to score
* must_not: Exclude documents


* term: Exact match 
* match: First analyzed

#### term level queries: 
 
* __term query:__ exact term 
* __terms query:__ any of the exact terms 
* __range query:__  values (dates, numbers, or strings) in the range
* __exists query:__ contains any non-null
* __prefix query:__ contains terms which begin with the exact prefix 
* __wildcard query:__ contains terms which match the pattern specified (single character wildcards (?), multi-character wildcards (*))
* __regexp query:__ contains terms which match the regular expression
* __fuzzy query:__ contains terms which are fuzzily similar (Levenshtein edit distance of 1 or 2)
* __type query:__ documents of the specified type
* __ids query:__ documents with the specified type and IDs

 
## Aggregation API

### Bucket Aggregation

```sh
# Terms
{
    "aggs" : {
        "genres" : {
            "terms" : { "field" : "genre" }
        }
    }
}
```

**Multi level Aggregation**

```sh
{
  "size": 0,
  "aggs": {
    "your_name": {
      "filter": {
	    "bool": {
	        "filter": [
	          {
	            "term": {
	              "yt_mcn": "2-blo8mnyjwH3ArQCGcf4g"
	            }
	          },
	          {
	            "term": {
	              "yt_country": "us"
	            }
	          }
	        ]}
	  },
      "aggs": {
        "yt_subscribers": {
          "sum": {
            "field": "yt_subscribers"
          }
        },
        "yt_subscribers_30_days_growth": {
          "sum": {
            "field": "yt_subscribers_30_days_growth"
          }
        }
      } 
    }
  }
}
```


### Metrics Aggregation
Calculate metrics over a set of documents


### Matrix Aggregation
Multidimensional Bucketing

### Pipeline Aggregation
Aggregations based on other aggregations and their associated metrics
 
## Debugging
 
### Logging

## Meta Fields

* dynamic (true, false, strict): Whether to automatically add new fields (strict => throw exception)
* \_source: 
* \_timestamp: 
* \_all: 

```sh
# Check Authenticated user and permissions:
GET  /_xpack/security/_authenticate
```
 

### [Hive Integration](https://www.elastic.co/guide/en/elasticsearch/hadoop/current/hive.html)




[Disable speculative Execution:](https://www.elastic.co/guide/en/elasticsearch/hadoop/master/configuration-runtime.html)

```sh
set hive.mapred.reduce.tasks.speculative.execution=false;
```

### Field Types 

* __text__ => full-text values, such as the body of an email or the description of a product

	Before ES5: `{"foo": {"type" "string", "index": "analyzed"} }`, 
With ES5: `{"foo": {"type" "text", "index": true } }`

* __keyword__: Full text content, such as for email addresses, hostnames, status codes, zip codes

	Before ES5:`{"foo": {"type" "string", "index": "not_analyzed"} }`
With ES5: `{"foo": {"type" "keyword", "index": true } }`


 
### Query-Types 

* __term__ => Looksup exact term in inverted index (case sensitive)
* __match__ => Applies analyzer to search (is case in-sensitive with default analyser)



### Export as CSV

```sh 
# Get Data
curl -s http://db:9200/index/_search\?size\=20 | jq -c '.hits.hits[]._source' > ~/Downloads/sample.json

# Get Schema
curl -s http://db:9200/index/_search\?size\=1 | jq -r '.hits.hits[]._source' | jq -r 'all_leaf_paths | join(".")' |  paste -sd, - > ~/Downloads/schema.txt

# Create CSV
cat ~/Downloads/schema.txt > ~/Downloads/export.csv
cat ~/Downloads/sample.json | json2csv -k $(cat ~/Downloads/schema.txt)  >>  ~/Downloads/export.csv 2> ~/Downloads/err.txt

# Check CSV with R
< ~/Downloads/export.csv Rio -e 'str(df)'
```


### Python without API

### Store multiple elements
```py 
import json
import http.client

conn = http.client.HTTPConnection("localhost", 9200)

with open('employee.json') as data_file:    
    headerInfo = {'content-type': 'application/json' }
    for emp in json.load(data_file):
        singleEmp = json.dumps(emp, sort_keys=True, indent=2)
        conn.request("POST", "/inca/employee?pretty",singleEmp, headerInfo)
        response = conn.getresponse()
```

### Query those Elements
```py 
import json
import http.client

conn = http.client.HTTPConnection("localhost", 9200)

with open('employee.json') as data_file:    
    headerInfo = {'content-type': 'application/json' }
    for emp in json.load(data_file):
        singleEmp = json.dumps(emp, sort_keys=True, indent=2)
        conn.request("GET", "/inca/employee?pretty",singleEmp, headerInfo)
        response = conn.getresponse()
```f