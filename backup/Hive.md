# HIVE SUMMARY


## Frequently used:

 - `hive --version` Check hive version 
 - `tail -fn 5000 hive.log`  listen logfiles, mostly at `/var/hive/` or `/tmp/logs/`

 
## Settings

```sh
# get all settings
set .
```

**Popular Settings:**

|Setting|Desc|
------------- |------------- |
`skip.header.line.count = 1`|to skip header line when importing a file|
`hive.cli.print.header=true;`|show column names |
`mapreduce.map.memory.mb=4096` `mapreduce.reduce.memory.mb=4096`|Increase Memory|
`hive.mapred.reduce.tasks.speculative.execution=false;`|show column names |

## Data Definition
- databaseet
- tables (internal/external)

```sql
CREATE TABLE test (
pstring STRING,
pint INT,
pdouble DOUBLE) 
row format delimited fields terminated by ','
STORED AS TEXTFILE;

describe [EXTENDED|FORMATTED] test
```
- partitions and buckets
- views

### Data Formats

|Description|Avro|JSON|ORC|Parquet
------------- |------------- |------------- |------------- |-------------
Origin|Cross-Hadoop|HTTP / JS|RCFile Replacement|Dremel Paper
Orientation|row|row|column|column
schema|segregated|integrated|footer|footer
type-model||rich|simple
compression|external|external|integrated|integrated
indexs|||yes|yes
stats||||yes 
Schema evolution||yes|coming(?)|only at the end 


[Benchmarks](http://de.slideshare.net/HadoopSummit/file-format-benchmark-avro-json-orc-parquet): 

 * Complex Tables with common strings: Avro with Snappy
 * Query a few columns on a wide dataset: Parquet
 * others: orc with Zlib is a good fit

## Data Selection and Scope

- JOINS (inner, left/right, outer)
- MAPJOIN (in-memory, small data size)
- UNION ALL

## Data Manipulation

- LOAD (text-import), INSERT (cross-table-movement) and EXPORT/IMPORT (backup, transformation to other DBS)
- ORDER (one reducer), SORT (at each reducer), DISTRIBUTE and CLUSTER (distribute+sort)
- Functions
  - Collection: SIZE
  - Date: 
  - Conditional: COALESCE, IF, CASE WHEN
    ` if `
- Transactions: ACID for ORC

## Data Aggregation and Sampling

- GROUP BY and GROUPING SETS (multiple groupings at a time)
- ROLLUP (n+1 levels of aggregation) and CUBE (all permutations [2^n])
- HAVING (WHERE on group)
- **Analytic functions**
  - RANK, DENSE\_RANK, ROW\_NUMBER
  - CUME\_DIST, PERCENT\_RANK
  - NTILE
  - LEAD, LAG
  - FIRST\_VALUE, LAST\_VALUE
- **Windows-clause**: ROWS BETWEEN <start\_expr> AND <end\_expr>
  - UNBOUNDED PRECEDING, CURRENT ROW, N PREDECING or FOLLOWING
- **Sampling**
  - Random: `SELECT * FROM <table> DISTRIBUTE BY RAND() SORT BY RAND() LIMIT <n>`
  - Bucket: `SELECT * FROM <table> TABLESAMPLE (BUCKET <bucket nr> OUT OF <total # buckets> ON RAND()`
  - Block: `SELECT * FROM <table> TABLESAMPLE (<N PERECENT | ByteLength | N ROWS>)`

## Performance

- Fasten queries using WHERE on partitioned columns

- **Utilities: Explain, Analyse**
   - AST (Abstract Syntrax Tree)
   - Stage dependencies
   - Stage Plan: Details on what happens when
- **Data Layout**: Partition, Buckets, Index
- **File Format:** 
	- ORC best for Hive (Hortonworks)
	- Parquett has greater portability (Cloudera)
	- Textfile are used for initialization (then INSERT into other table)
- Compression: LZ4 or Snappy
- Storage Optimization
- SerDe: Changing data on input/output

### Performance Recomandations from Bernhard Schäfer:

**Dateiformat**

* Write Once - Read Many und Datengröße > 1-2 HDFS Blöcke: Parquet/ORC
* Write Once - Read Once oder sehr kleine Daten: Avro

**MapReduce:**

* Intermediate Compression wenn Query in mehrere MR Jobs kompiliert wird: LZO oder Snappy
* Mapper Memory erhöhen (mapreduce.map.memory.mb und mapreduce.map.java.opts), um ggfs. Joins in MapJoins zu konvertieren
* Sehr viele Minitasks (Laufzeit < 30 Sekunden): hive.exec.reducers.bytes.per.reducer / mapreduce split size anpassen
* Kleine Queries als Ubertask in Application Master: set mapreduce.job.ubertask.enable=true

**Query Formulierung:**

* Wenn Cost-based Optimizer bei sehr großen Queries (viele Subqueries) versagt, Query ggfs. aufsplitten
mit CREATE TEMPORARY TABLE STORED AS AVRO AS Zwischentabelle erstellen
* Lohnt sich vor allem wenn MR als exec. engine genutzt wird

**Statistics**

* Essentiell für Cost-based Optimizer
* Im Hiveserver Log ist ersichtlich, ob der Cost-based Optimizer erfolgreich war oder nicht
* Wenn nein, lieg es z.T. an fehlenden Statistiken zu Partitionen/Tabellen
* Diese ggfs. manuell mit ANALYZE TABLE erzeugen
* Bei Tabellen die sich selten ändern ggfs. auch Statistiken auf Column Ebene berechnen lassen

Weitere Settings:

* `set hive.exec.parallel=true;`
* `set hive.exec.mode.local.auto=true;` : Queries auf kleinen Datenmengen direkt im Hive Server ausführen lassen (ACHTUNG: HiveServer2 Last monitoren)


### Programming Hive Performance Tips :

Table Size | Data Profile | Query Pattern | Recommendation
----|----|----|----|
Small | Hot data | Any | Increase replication factor
Any | Any | Very precise filters | Sort on column most frequently used in precise queries
Large | Any | Joined to another large table | Sort and bucket both tables along the join key
Large | One value >25% of count within high cardinality column | Any | Split the frequent value into a separate skew
Large | Any | Queries tend to have a natural boundary such as date | Partition the data along the natural boundary

## Extensibility

- UDF, UDAF (Aggregatin), UDTF (Table-Generating)
- Deployment: 
  - Write Java code and compile it as JAR
  - Add JAR to HIVE (`ADD JAR {path}`) 
  - Create a function temporary (`CREATE TEMPORARY FUNCTION {name} AS {package-class-spec};`) or permanently (`CREATE FUNCTION {name} AS {package-class-spec} USING JAR '{hdfs-path}'`)
  - Check function (`SHOW FUNCTION {name};`) and use in HQL
  - Create Complex Types
	  	- Array <data_type>
	  	- Map <primitive_type, data_type>
	  	- Struct: <col_name: datatype,...>
	  	- Uniontype: <data_type, data_Type, ...>
 
**Create Complex Types**

``` java
map(key1,value1,key2,value2)
array(val1, val2)
size(Map<K.V>)	
```
 
**Create UDF**

```java 
package com.yourpackage.hiveudf;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.udf.UDFType;

@Description(
name = "udf_name",
value = "_FUNC_(arg1, arg2, ... argN) - A short description for the function",
extended = "This is more detail about the function, such as syntax, examples."
)
@UDFType(deterministic = true, stateful = false)
public class udf_name extends UDF {
     public String evaluate(<Type_arg1> arg1,..., <Type_argN> argN){
          /*
           * Do something here
           */
          return "return the udf result";
     }
}
```

## Security
 - Authentication
 - Authorization: Legacy (simple), Storage-based (HDFS), SQL (column and row level)
 - Encryption: Not out-of-the-box, custom UDFs required


10) Other Tools:

``` sql 
SELECT count(distinct ws1.ws_order_number) as order_count,
       sum(ws1.ws_ext_ship_cost) as total_shipping_cost,
       sum(ws1.ws_net_profit) as total_net_profit
FROM web_sales ws1
JOIN /*MJ1*/ customer_address ca ON (ws1.ws_ship_addr_sk = ca.ca_address_sk)
JOIN /*MJ2*/ web_site s ON (ws1.ws_web_site_sk = s.web_site_sk)
JOIN /*MJ3*/ date_dim d ON (ws1.ws_ship_date_sk = d.d_date_sk)
LEFT SEMI JOIN /*JOIN4*/ (SELECT ws2.ws_order_number as ws_order_number
                          FROM web_sales ws2 JOIN /*JOIN1*/ web_sales ws3
                          ON (ws2.ws_order_number = ws3.ws_order_number)
                          WHERE ws2.ws_warehouse_sk <> ws3.ws_warehouse_sk) ws_wh1
ON (ws1.ws_order_number = ws_wh1.ws_order_number)
LEFT SEMI JOIN /*JOIN4*/ (SELECT wr_order_number
                          FROM web_returns wr
                          JOIN /*JOIN3*/ (SELECT ws4.ws_order_number as ws_order_number
                                          FROM web_sales ws4 JOIN /*JOIN2*/ web_sales ws5
                                          ON (ws4.ws_order_number = ws5.ws_order_number)
                                          WHERE ws4.ws_warehouse_sk <> ws5.ws_warehouse_sk) ws_wh2
                          ON (wr.wr_order_number = ws_wh2.ws_order_number)) tmp1
ON (ws1.ws_order_number = tmp1.wr_order_number)
WHERE d.d_date >= '2001-05-01' and
      d.d_date <= '2001-06-30' and
      ca.ca_state = 'NC' and
      s.web_company_name = 'pri';
```

 
``` sql 
alter table <tab> set tblproperties ('<prop>' = '<val>')
show tblproperties <tab> ('<prop>')
```


## Clients

### Hive CLI
```sh
# connect
hive -h <SERVER> --database <DATABASE> 

# Execute script
hive -f <file-path-to-script>  
```

### Beeline
```sh
# connect
beeline -u jdbc:hive2://<SERVER>:10000/<DATABASE> -n <USER> -p="<PW>"
```
  