# OS-Utilities

## Shell FreqUsed
```sh
# Change Windows line-endings to Linux
perl -pi -e 's/\r$//' ~/.tmux.conf 

# Show destination of alias
type -a sqlite

# show script behind command
whereis <CMD> 

# create directory recursively 
mkdir -p foo/bar/zoo/andsoforth

# work as root
sudo -i

# Debugausgabe (de)aktivieren
set +x / -x 

# Forkbomb (Crash system recursively)
:(){ :|:& };:
```

### grep

```sh
# grep OR  
grep 'pattern1\|pattern2' filename

# grep AND
grep -E 'pattern1.*pattern2' filename # with order
grep -E 'pattern1.*pattern2|pattern2.*pattern1' filename # without order
```

### awk
```sh
# Skip first line
awk 'FNR > 1'

# print first column
awk '{print $1}' 

# Combine multiple lines to comma speratd string
paste -s -d, -
```

### sed
```sh

```


## Processes

[Exit Codes With Special Meaning](http://www.tldp.org/LDP/abs/html/exitcodes.html)

```sh
# Signals
SIGINT	2	# Terminal interrupt (Ctrl + c
SIGKILL	9	# Kill(can't be caught or ignored) (POSIX)
SIGTERM	15	# Termination (ANSI)

# Show process
ps faux

echo "myCommand" | $? # Check signal received

# Remove file 
[ -e /path/file ] && rm /path/file || true  # Exit Code 0
```

## Package management

```sh
# apt-get install
apt-get update && apt-get install -y 
net-tools         # netstat
curl              # curl
dnsutils          # dig
docker.io         # docker
netcat            # netcat
procps            # ps aux
```

## Access System Information

__Software__

```sh
uname -a # general system information
cat /etc/*-release # Show Linux Version

printenv # print all System variables
apt list --installed # Show installed packages
dpkg --get-selections | grep -v deinstall # List all installed Software on Ubuntu

cut -d: -f1 /etc/passwd # List all users
service --status-all # Show all running services (+ started, - stopped, ? unknown)

# Show shell version running
echo $0
```

__Hardware__

```sh
hwinfo --short # Hardware summary (extra package)
lscpu # CPU information
lshw # general Hardware information

more /proc/cpuinfo # CPU
cat /proc/meminfo | grep MemTotal | awk '{ print $2 }' # RAM
```

### Dateisystem
```sh
df -h # Show storage on all mounted devices
du -sh ./* | sort -nr | head -n 20 # Show size of all direct children
lsblk -io KNAME,TYPE,SIZE,MODEL # Show list of block devices

# Quick Search
locate filename

# search json files (specific type)
find $directory -type f -name "*.json"

# search folder
find / -type d -name 'httpdocs'

# search folder but don't show errors
find /* -type d -name "lib"  2>/dev/null
```

**Create a new filesystem**

```sh
pvs  / vgs / lvs # show physical volumes / logical volume groups / logical volumes
pvdisplay # show information about physical volumes
vgs # show logical volumes

vgdisplay # Show information about open spaces
lvcreate -L 50G vg00  # create logical volume with 50GB
vgdisplay -v vg00 # Show the new created volume

parted /dev/mapper/vg00-lvol0 # create partition
>> mklabel gpt                # Define GUID Partition Table as partition type
>> mkpart xfs 0% 100%         # Define filesystem and percentage of usage

mkfs.xfs /dev/mapper/vg00-lvol0p1 # Create a file system from device
vim /etc/fstab # Add new partition to file system table
mkdir /export/data # Create a folder where the filesystem shall be mounted
mount -a # mounts all devices according to fstab

```

__Extend existing partition__

```sh
lvextend -L 6G /dev/mapper/vg00-usr
xfs_growfs /usr -D 100%

lvresize -r L5G /dev/vg00/usr
```

### Network

#### General information

```sh
ip addr # Get IP address

nettop # General overview
```

#### Debugging from inside

```sh 
# find ports to processes
netstat -tulpn 
- t / u # show tcp / udp
- l # list only listening processes
- p (e)# show processes/pid (and user owning the process)
- n # 
- ct # show outpout continously

# Get process runing on port 9200
sudo lsof -n -i :9200 | grep LISTEN

# Find ports a process listing on
lsof -Pan -p <PID> -i

# Test connection on a port
nc -l <port> # on server
nc -vz <serverIP> <port> # on client

# Determine if programs listen to ports
nmap -T4 -A -v -PE -PS22,23 -PA22,23 <serverIP> 

# Scan ports
nmap -T Aggressive -A -v 127.0.0.1 -p 1-65000

# Dump all TPC traffic which is not SSH
tcpdump -i any port not 22

# Dump all traffic from host on interface
tcpdump -i ens3 -n src host 192.168.0.10

```


#### Debugging from outside

```sh 
# Lookup DNS
dig <address>

# Check own IP Address
dig +short myip.opendns.com @resolver1.opendns.com

# Check connection to host
telnet host port

wireshark

strace
```

#### OSI / TCPIP Layers & Protocols

```sh
# layer7 = Application Layer
HTTP
NTP: Network Time Protocol for clock synchronization
FTP: File Transfer
IMAP: Internet Message Access Protocol for emails
Telnet: Bidirectonal interactive text-oriented communication using virtual terminal connection

# layer6 = Presentation Layer

# layer5 = Session Layer

# layer4 = Transport Layer
TCP: reliable, ordered and error checked delivery of stream of octets (8 bits)
UDP: connectionless transmission model with checksums and ports, but without ordering and protection of duplicates or order

# layer3 = Network / Internet Layer
Transferring datagrams (data sequences of variable length) => IP

# layer2 = Data Link (IEEE 802)
LLC (Logical link controll): Enables multiplexing on Layer1
MAC (Media access controll): Provides adressing and channel access controll => Ethernet, LAN 

# layer1 = Physical 
cable, optical fiber, radio frequency..

# Other
VLAN: uses tags to coordinate network packets to network components 
VxLan (Virtual Extensible LAN): Encapsulates MAC-based Ethernet frames within UDP packets in a VLan style
```


#### SSH Tunnel
```sh
ssh -L 9000:imgur.com:80 user@example.com
ssh -nNT -L 9000:imgur.com:80 user@example.com # Port Forwarding without tty

## SSH Tunnel and allow remote hosts to connect to local forwarded ports
ssh -L 1234:localhost:9200 user@sshConnection -g

# socks proxy via ssh aufmachen und unter localhost:6677 als SOCKS proxy eintragen
ssh -D 6677 user01@146.148.123.164

# Agent forwarding
ssh-add ~/.ssh/id_rsa
ssh -tA 
```

## SSL

```sh
# Debug existing connection / Show Certificate
openssl s_client -connect test.de:443 -prexit

# Debug existing connection / Show Certificate
openssl s_client -connect test.de:443 -prexit
```



```sh
# Create Key 
openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365 -nodes -subj '/CN=localhost'
```

* __req__ PKCS# 10 certificate request and certificate generating utility.

* __x509__
this option outputs a self signed certificate instead of a certificate request. This is typically used to generate a test certificate or a self signed root CA.

* __newkey ..__
this option creates a new certificate request and a new private key. The argument takes one of several forms. rsa:nbits, where nbits is the number of bits, generates an RSA key nbits in size.

* __keyout ..__
this gives the filename to write the newly created private key to.

* __out ..__
This specifies the output filename to write to or standard output by default.

* __days ..__ number of days to certify the certificate for

* __nodes__ private key will not be encrypted.


## Spoof / Change mac address

```sh
openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'
sudo ifconfig en0 ether xx:xx:xx:xx:xx:xx
```


### Mail

```
swaks
```

## SSH

__Create Key__

```sh
ssh-keygen -t rsa -b 4096 -C "your_email@example.com" # Create key-pair
ssh-keygen -p  # To change passphrase on existing key
```

__Login__

```sh
ssh -D 6677 user@host # Open one specific port
ssh -L 9000:localhost:8080 user@host # Open SOCKS-Proxy (access via FoxyProxy)
```

__Mount file__

```sh 
sshfs USER@192.168.178.32:/home/USER ~/remote
cp -p someFile ~/remote/put/it/some/where/oh/damn/you/here
```

__Configuration__

```sh
# entries in .ssh/conf
HOST target.machine
	User abcd
	IdentitiyFile ~/.ssh/id_rsa_alternative
	ProxyCommand ssh abcd@proxyserver %	
	HostName      target.machine
	
# using proxy machine
	ProxyCommand  ssh proxyuser@proxy.machine nc %h %p 2> /dev/null

# Add custom DNS Hosts
echo "ip-address dns-name alias" | sudo tee a /etc/hosts
```



## transport data

```sh
find . -type f -name "*_2016_*" | tar -czvf backup.tar.gz -T -
```


### SCP

```sh 
# Copy matching logs to local directory
scp -p myserver:/home/myuser/log/\*\.2017-01-22\.log .

# Transport Data over intermediate host
scp -3 user@source:path user@dest:path
```



### Curl

```sh 
# load multiple files at once
curl https://www.youtube.com/channel/{UCqPkhSQTtmXu1FTtjVZXLFw,UCqItFEumlRmIk4EJU4OvuQA}/about

# load range of files at once
curl --fail -k "https://gallery/pics/IMG_[6900-9000].JPG" -o "# 1.jpg"

# Check response (devlopment mode)
curl -vs addres

# Get Status code
curl -o /dev/null --silent --head --write-out '%{http_code}\n' <url>

# Login and store cookie
curl -c cookie.txt -d "email:<your-mail>" -d "password:<your-pw>" https://somepage/login

# Use Cookie (also for redirects)
curl -L -b cookies.txt https://targeturl
```

### [Timing Curl](https://blog.josephscott.org/2011/10/14/timing-details-with-curl/)

``` sh
curl -w "@~/.files/curl-format.txt" -o /dev/null -s http://wordpress.com/
```

with curl-format.txt:

``` sh
            time_namelookup:  %{time_namelookup}\n
               time_connect:  %{time_connect}\n
            time_appconnect:  %{time_appconnect}\n
           time_pretransfer:  %{time_pretransfer}\n
              time_redirect:  %{time_redirect}\n
         time_starttransfer:  %{time_starttransfer}\n
                            ----------\n
                 time_total:  %{time_total}\n
```

### wget
```
# Output file


```


## File Structure

* etc: Configuration Files
* dev: Device Files
* var: Variable Files
* usr: User Files
* home: Home Directories
* mnt: Mount Directory
* opt: optional (all kind of additional stuff programs need)

_/bin_ = normal executables  
_/sbin_ = system administration commands

### Set File Permissions

```sh
chmod rwx r-x r-- info.sh # using 
chmod 754 info.sh # using binary format
0 None / 1 X / 2 W / 3 WX / 4 R / 5 RX / 6 RW / 7 RWX

chmod a+x filename # add execute to all 
chmod u+r,g-x filename # add read to user, remove execute to groups
chmod --reference=info.sh file2 # give file2 same permissions
```

## User-Right Management
```sh
# Allow a user to access a service without entering a pw
sudo visudo
>> %privilegedUser ALL= NOPASSWD: /usr/bin/service

# list all users
cat /etc/passwd 

# Create user
useradd rakhi
```

## User Group Management
```sh
groupadd bdhcore
useradd -G developers vivek # add vivek to developers group

# Add active user to group
usermod -a -G groupName $USER 

# List all users who are in group
getent group docker

groups root # list all groups of user root
```

# Linux-Distributions

## Container-Linux

__Components__

* Flannel: Networking option
* [Locksmith](https://github.com/coreos/locksmith): Reboot manager

# OsX

```sh
# List Network Hardware 
networksetup -listallhardwareports

# Trace TCP Network traffic
sudo tcptraceroute <my-ip> 443

sysctl -n machdep.cpu.brand_string

system_profiler

# copy to clipboard
pbcopy

# paste from clipboard
pbpaste
```