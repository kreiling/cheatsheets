# Hadoop Ecosystem

## View Logs

Logs: `/var/log/hadoop-yarn`
Settings: `/etc/hive/conf`


## Distcp

```sh
# Copy all files into destination folder
hadoop distcp hdfs://nn_src:50070/path/year=2017/month=1/day=1 hdfs://nn_dest:8020/user/<USERNAME>/folder/table/year=2017/month=1/

# Replace destination folder with src folder
hadoop distcp -update hdfs://nn_src:50070/user/<USERNAME>/folder/table/year=2017/month=1/day=1 hdfs://nn_dest:8020/user/<USERNAME>/folder/table/year=2017/month=1/day=1
```


## Setup Hadoop high availability Cluster
1. Install Java
2. Install journals
3. install zookeeper
4. install namenode

	1. For Both Namenodes: create namenode and data dirs
	2. On Active-Namenode: 
	
		```
		start hadoop-hdfs-zkfc
		hdfs namenode -format
		/etc/init.d/hadoop-hdfs-namenode start
		```
	
	3. On Inactive-Namenode: 
	
		```
		hdfs namenode -bootstrapStandby
		su hdfs
		/etc/init.d/hadoop-hdfs-namenode start
		```
	

## HDFS
Nutzerberechtigungen werden im Namenode nachgeschlagen

```sh
# upload file
hadoop fs -put /<local machime path> /<hdfs path>
# download file
hadoop fs -get /<hdfs path> /<local machime path>

# size per directory
hadoop dfs -du -h -s [directory]/*

# Check available space
hdfs dfsadmin -report 
```

## Security Frameworks

* [Keberos] (https://hadoop.apache.org/docs/r2.7.2/hadoop-project-dist/hadoop-common/SecureMode.html# Data_Encryption_on_RPC)
* [knox](https://knox.apache.org/)
* [ranger](http://ranger.apache.org/)
* [recordservice](http://recordservice.io/overview/)
