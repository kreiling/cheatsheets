# Hackr Tools

## tmux

__Startig tmux__

```sh
tmux new -s name # create named tmux session
tmux attach -t 0 # attach the first session of tmux ls
```

__tmux commands__

```sh
d # detach
z # fullscreen view
% " # create panes
c # create windows
p n # switch to previous / next window
[ # enter copy mode (space: copy, enter: finish copy)
] # paste 

:source-file ~/.tmux.conf # reload tmux.conf
:swap-window -s 3 -t 1
:setw synchronize-panes on # run command in all panes
```

__commands based on my tmux.conf__

```sh
e # run command in all windows
a # toggle synchronize-panes
p # Copy tmux clipboard to default one
s # save settings
r # recover last settings
```

# less

```sh
-L      # show line numbers
f / b   # move window-wise 
d / u   # move half window-wise
/       # Serach
n       # next search result)
g / G   # Jump to first / last line:
# Find brakets by typing the brakets
```


## xdotool

```sh
xdotool type mytext
```

## htop

[CheatSheet](https://peteris.rocks/blog/htop/)

Shortcut  | Command
------------- | -------------
/  | Search
\  | Filter
t  | Tree View
+ / - | Expand / Collapse subtree
k  | Kill Process
Space | Tag/Untag a process
U  | Untag all processes


Shortcut  | Sort by
------------- | -------------
M  | Memory usage
P  | Processor usage
T  | Time
F  | Follow a process
K  | Hide kernel threads
H  | Hide user threads


## zsh


```sh
# execute sudo without password prompt
echo myPassword | sudo -S ls /tmp
```