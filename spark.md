# Apache Spark

``` sh
# interactive spark shell standalone mode
spark-shell --master ip:host

# load packages on runtime
spark-shell --packages org.apache.spark:spark-streaming_2.11:2.1.0 --packages org.apache.bahir:spark-streaming-twitter_2.11:2.1.0

:paste

# submit spark job
spark-submit --master $SPARK_MASTER --jars $DEPENDENCIES --class me.spark.streaming.xxx targets/myjob-1.0.0.jar

# interactive spark shell started with 2 cores locally
spark-shell --master local --jars code.jar

# Print all defined variables
$intp.definedTerms.map(t => s"${t.toTermName}: ${$intp.typeOfTerm(t.toTermName.toString)}").foreach(println)
```

## Hello world (scala)

```scala
/* distributed hello-world */
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

val conf = new SparkConf().setAppName("Simple Application")
val sc = SparkContext.getOrCreate()

val html = scala.io.Source.fromURL("https://spark.apache.org/").mkString
val list = html.split("\n").filter(_ != "")
val rdds = sc.parallelize(list)
val count = rdds.filter(_.contains("Spark")).count()
```

```scala
val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount").set("spark.ui.port", "44040" ).set("spark.driver.allowMultipleContexts", "true")
val ssc = new StreamingContext(conf, Seconds(2))
```

## Streaming 

```scala 
// Stop streaming context
StreamingContext.stop(stopSparkContext=false, stopGracefully=true)
StreamingContext.stop()
```