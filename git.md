# git


## Git Featurebranch workflow

``` sh
git branch feature/123
git checkout feature/123
# make changes and commit normally

git checkout develop
git merge --no-ff feature/123
git branch -d feature/123
git push origin develop
```

## Deploy release
1. Checkout dev and pull changes
2. merge to master
3. Create Release tag `git tag -a <Major.minor> -m "most important changes"`

## Git-History

```sh
# List changed files
git diff-tree --no-commit-id --name-only -r bd61ad98
git show --pretty="" --name-only bd61ad98 
```

[StackOverflow](http://stackoverflow.com/questions/424071/how-to-list-all-the-files-in-a-commit)

## Git squash last X Commits

[StackOverflow](http://stackoverflow.com/questions/5189560/squash-my-last-x-commits-together-using-git/5201642# 5201642)


``` sh
git reset --soft HEAD~3 && git commit 
```

## Reset last false commit

``` sh
# only local
git reset HEAD~  

# remote
git push origin +ae8ac3e0^:master
```

## List ignored files and rules
``` sh
git check-ignore -v *
```

## Revert already pushed commits

``` sh
git revert --no-commit D
git revert --no-commit C
git revert --no-commit B
git commit -m 'the commit message'
```

## Remove old branches which no longer exist [Github](http://stackoverflow.com/questions/7726949/remove-local-branches-no-longer-on-remote?answertab=oldest# tab-top)

``` sh
# Remove remote branches
git remote prune origin

# Remove merged local branches (having oportunity to edit list before removal)
git branch --merged | grep -v "\*" | grep -v "master" | grep -v "develop" | grep -v "staging" >/tmp/merged-branches && vi /tmp/merged-branches && xargs git branch -d </tmp/merged-branches
```

## Work with submodules

```sh
git submodule status
git submodule 
```

## Check history

``` sh
# Check history
git log --name-status

# Follow changes of a single file
gitk --follow [filename]
git log -p filename

# See which line got changed when and by whom
git blame 
# Only check line 47 and commits before 0eeba1ef
git blame -L47 0eeba1ef^ -- create-cluster.sh
```