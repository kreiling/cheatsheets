# JQ

## Basic Operators

```sh
.<Attr> # Select attribute 
[i] # Select element of array
.[] # wildcard
.level1 | .. | objects | .level3 | select(. != null) # select all level 2 attributes
-r # or --raw-output (remove double quotes)
|= # make assignment 
```

```sh
# use system variables / pass function parameter
jq --arg context "$context" '.contexts[] | select(.name==$context) | .context.cluster' 

# Concat multiple fields
jq '.users[] | .first + " " + .last'
jq '.servers[] | "\(.properties.name) \t \(.entities.volumes.items[0].properties.type)"'

# create new objects
jq '._source | {ip: .ip,name: .name}'
```

## Filters

```sh
# Filter array based on elements attribute
jq '.[] | select(.id == "second")' 

# Filter object by attribute
jq . - map(select(.Names[] | contains ("data"))) | .[] .Id

# Get nth element
[<your actual selector>][n]'
[.[] | select(.category == "database")][n]' # Example

# join multiple attribute (create Zeppelin table)
jq -r '.body | [to_entries[] | .value.id + "\t"+ .value.name ] | join("\n") ')

# JQ with xargs for each element
jq -r '.[].id' | xargs -L1 echo 1
```

## Modifiers

```sh
# Apply action to each el in array
jq 'map(.+1)'

# Delete attribute
jq  'del(.metadata.resourceVersion)'

# 
jq 'setpath(["imagePullSecrets"];[{"name":"myregistrykey"}])'
```

## Others

[Convert object properties to array](http://stackoverflow.com/questions/28615174/jq-filter-on-sub-object-value)

```sh
jq 'with_entries(select(.value.category == "big-data"))'
jq 'to_entries | .[] | select(.value.category == "big-data")'
```