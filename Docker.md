# Docker

## Dockerfile

### [Executeable commands](https://www.ctl.io/developers/blog/post/dockerfile-entrypoint-vs-cmd/)

* `CMD`: For often overwritten things such as (`/bin/bash` or `/bin/sh`)

* `ENTRYPOINT`: For situations the CMD command should be prepended.

both can use two versions of execution (just replace CMD with ENTRYPOINT):

* `CMD ["executable","param1","param2"]`

* `CMD executable param1 param2` will trigger: `/bin/sh -c 'executable param1 param2'` and therefore result in something like: 

```sh
UID PID PPID C STIME TTY TIME CMD
root 1 0 0 20:14 ? 00:00:00 /bin/sh -c ping localhost
root 9 1 0 20:14 ? 00:00:00 ping localhost
``` 

Use both for cases like 

```sh
ENTRYPOINT ["/bin/ping","-c","3"]
CMD ["localhost"]
```

```sh
# Overwrite entrypoint in cli using:
docker run -it --entrypoint=/bin/sh ubuntu
```



### Expose vs ports

* `expose`: port is accessible within the docker network and can be used together with `link`
* `-p` (docker automatically adds `expose`): port is accessible even from outside the containers

```sh
# port mapping
-p hostPort:containerPort
```

### Volumes

```sh
# mount directory
-v /host/dir:/container/dir

# mount local directories / relative path (using pwd)
-v $(pwd)/src:/src 
```

## Installation

**On an Ubuntu Host**

```sh
# Install docker on ubuntu
sudo apt-get -y --no-install-recommends install curl  apt-transport-https  ca-certificates  curl  software-properties-common
curl -fsSL https://apt.dockerproject.org/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb https://apt.dockerproject.org/repo/ \
   ubuntu-$(lsb_release -cs) \
   main"
sudo apt-get update && apt-get -y install docker-engine

# Add user to group docker
sudo gpasswd -a ${USER} docker
sudo usermod -aG docker $(whoami)
```

## Network

### Handle vpn VPN

__Option 1: network=host__

```sh
# Create local-socket
ssh -L 1234:localhost:9200 user@sshConnection

## Start container with network=host
docker run -it --net=host --rm alpine /bin/ash

## Get default gateway in container
netstat -nr | grep '^0.0.0.0' | awk '{print $2}' # or
ip route # default via <xx.xx.xx.xx>

# get data
curl <defaultgateway>:1234
```

__Option 2: bind additional ip to host__

```sh
# Bind additonal ip to loopback-interface (localhost)
sudo ifconfig lo0 alias 10.200.10.1/24

# Create local-socket and bind it to gerneated ip
ssh -L 10.200.10.1:1234:localhost:9200 user@server

# Start container with normal bridge Network
docker run -it --rm alpine /bin/ash

# Get data
curl 10.200.10.1:1234
```


## Shortcuts / Utilities


```sh
# Get all container names
docker ps -a | awk '{if(NR>1) print $NF}'

# Get access to root shell
docker exec -it --privileged  -u 0 containername bash 

# Run container endless / infinitly (using a dummy process)
docker run -d --entrypoint sleep ubuntu infinity

# run alpine container infinitly
docker run -d --entrypoint=/bin/ash alpine -c 'while sleep 3600; do :; done'
```


### Cleanup

```sh
# Stop + Remove all docker containers
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)

# Remove multiple docker containers
docker ps | grep <my-container-name> | awk '{print $1}' | xargs -L1 docker rm -f 

# Remove all Docker images
docker rmi $(docker images -q |  tr '\n' ' ')

# Cleanup containers, volumes, networks and dangling images
docker system prune
```