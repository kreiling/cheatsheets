# Maven

```
mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:get     \
-DrepoUrl=https://mvnrepository.com/artifact/     \
-Dartifact=com.datastax.spark:spark-cassandra-connector_2.10:2.0.0-M3



mvn dependency:purge-local-repository    \
-Dinclude=com.datastax.spark:spark-cassandra-connector_2.10:2.0.0-M3
```

# SBT

```scala
// Project name (artifact name in Maven)
name := "(project name)"

// orgnization name (e.g., the package name of the project)
organization := "com.treasure-data"

version := "1.0-SNAPSHOT"

// project description
description := "Treasure Data Project"

// Enables publishing to maven repo
publishMavenStyle := true

// Do not append Scala versions to the generated artifacts
crossPaths := false

// This forbids including Scala related libraries into the dependency
autoScalaLibrary := false

// library dependencies. (orginization name) % (project name) % (version)
libraryDependencies ++= Seq(
   "org.apache.commons" % "commons-math3" % "3.1.1",
   "org.fluentd" % "fluent-logger" % "0.2.10",
   "org.mockito" % "mockito-core" % "1.9.5" % "test"  // Test-only dependency
)
```


```
name := "Twitter Streaming"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
   "org.apache.spark" %% "spark-core" % "1.2.0",
   "org.apache.spark" %% "spark-streaming_2.11" %% "2.1.0",
   "org.apache.bahir" %% "spark-streaming-twitter_2.11" %% "2.1.0"
)
```