# R Cheatsheet

## Basics

Overview Commands

```r
nrow(df)
colnames(df)
str(df)
summary(df)
head(df)
summary(as.numeric(df$price))
```


## DataFrames

```r
df = data.frame(summary(alle$start))
rownames(df)
df$yt_vertical <- as.factor(df$yt_vertical) # String to Factor

subset(df,col1>4) # subset rows
df = export[,c("col1","col4")] # get cols
```

```r
merged = merge(df,cities)
summary(merge(df,cities))
```


## Working with Dataframes

```r
# Sort Dataframe
dd[with(dd, order(-z, b)), ]
```


## Plotting


```r
plot(summary(cities[,2]))
plot(df$my_factor, df$my_num) # boxplot
plot(df$yt_vertical, las=2) # rotate labels
```

```r
par(mar=c(5,10,2,2))
plot(summary(cities[,2]),1:21,yaxt="n",ylab="")
axis(2, at=1:length(unique(cities[,2])),labels=unique(cities[,2]), las=1)
```


```r
parseCsv = function(path){
  df = read.delim(path, header=FALSE) # Read CSV
  colnames(df) = c("history_action_id","ts_init","init_date_key","init_date_id","ts_lastchange")
  df$start_date = as.Date(as.POSIXct(df$ts_init, origin="1970-01-01")) # Convert to  date
  return(df)
}


loadFolder = function(folder="~/datafolder"){
  temp = list.files(path=folder, pattern="*.csv") # Load all files
  for (i in 1:length(temp)) {
	assign(temp[i], parseCsv(paste(folder,temp[i],sep="/")))
  }
  return (temp)
}

```