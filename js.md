# JavaScript

## Async

### Q

```js 
var items = [1, 2, 3, 4, 5];
var fn = function asyncMultiplyBy2(v){ // sample async action
    return new Promise(resolve => setTimeout(() => resolve(v * 2), 100));
};
// map over forEach since it returns

var actions = items.map(fn); // run the function over all items.

// we now have a promises array and we want to wait for it

var results = Promise.all(actions); // pass array of promises

results.then(data => // or just .then(console.log)
    console.log(data) // [2, 4, 6, 8, 10]
);
```


## Node

``` sh
# update node itself
npm update npm -g 
```

``` sh
# Clean install node package
rm -rf node_modules/                               
npm cache clean
npm install
```

## Other

### Inject jQuery in existing page

```js
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js';
document.head.appendChild(script); 
$('meta[name=attribution]')
```


### Test cors from console
```js
var xhr2 = new XMLHttpRequest();
xhr2.open('POST', 'http://<endpoint>:9200/my-index/_search?q=my-query', true);
xhr2.setRequestHeader('Content-Type', 'application/json');
xhr2.send()
```
