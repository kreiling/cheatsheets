# vi(m)


**Fast Lookup:**

```sh
yy          # copy line 
u           # undo
:%s/,/\r/g  # replace each , with a new line: 

x / dw / dd # Delete Character / word / line :
d$          # Delete all till end of line

5d          # remove line 5
5dd         # remove lines till line 5

:m +1 / -2  # move down / up one line

```

**Cursor Motion:**

```sh
hjkl    # Move cursor
L / M / H # Cursor to bottom / middle / top line
:\<NR>  # Go to file xy
w, e, b # start of next word, end of this word, backwards
2w      # jump to word forward
^       # Back to indentation    
0, $:   # start / end of the line
```

**Page Motion:**

```sh
gg               # Go to beginning of file
gG               # To to end of file
:                # Goto line

C-d              # Half page down
C-u              # Half page up
C-f              # Next page
C-b              # Previous page
C-Down or J      # Scroll down
C-Up or          # Scroll up
```
   
**Search:**
     
```sh
/         # Search forward          
n         # Search again            
?         # Search backward         
```

** Text Replication :**

```sh
:s/foo/bar/g		# Change each 'foo' to 'bar' in the current line.
:%s/foo/bar/g		# Change each 'foo' to 'bar' in all the lines.
:5,12s/foo/bar/g	# Change each 'foo' to 'bar' for all lines from line 5 to line 12 (inclusive).
:'a,'bs/foo/bar/g	# Change each 'foo' to 'bar' for all lines from mark a to mark b inclusive (see Note below).
:'<,'>s/foo/bar/g	# When compiled with +visual, change each 'foo' to 'bar' for all lines within a visual selection. Vim automatically appends the visual selection range ('<,'>) for any ex command when you select an area and enter :. Also, see Note below.
:.,$s/foo/bar/g		# Change each 'foo' to 'bar' for all lines from the current line (.) to the last line ($) inclusive.
:.,+2s/foo/bar/g	# Change each 'foo' to 'bar' for the current line (.) and the two next lines (+2).
:g/^baz/s/foo/bar/g	# Change each 'foo' to 'bar' in each line starting with 'baz'.
```

**Selection:**
     
```sh
Space     # Start selection         
Escape    # Clear selection         
Enter     # Copy selection          
p         # Paste buffer            
```

<kbd>:%y+</kbd> Copy full file to clipboard

![Vi Cheat Sheet](img/vicc.gif)

```
set :nowrap
```