<img src="http://www.edvotek.com/c.ACCT125049/site/img/NEWS_9.28.15_Targets_Fig1.jpg" style="float: right" height="250px" />

# Data Science

## General Stuff

  | real  |	 fake
----| ---- | ----
guess yes| tp | fp
guess no| fn | tn

__Metrics:__

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Precisionrecall.svg/700px-Precisionrecall.svg.png" style="float: right" height="400" />

* **Precision:** How many positive predicted events are real? `tp / (tp + fp)`
* **Accuracy:** Percentage correctly predicted `tp +  tn / ( P + N)`
* **Recall:** How many true events correctly predicted? `tp / (tp + fn)`
* **F1:** `2 * (prec * rec) / (prec + rec)`



## Datasets

* [Usa.gov](https://github.com/usagov/1.USA.gov-Data)

* [Movielens](http://grouplens.org/datasets/movielens/) - 1M Ratings on movie with usefr information

* [Baby Names in the US](https://www.ssa.gov/oact/babynames/limits.html)

### R / Pandas Comparison

R| pandas | SQL
----| ---- | ----
`dim(df)`|` df.shape` |
`head(df)`|` df.head()` | `LIMIT 5`
`nrow(df)`|` df.shape()[0]` |
`str(df)`|` df.info()` | 
`summary(df)`|`df.describe()` | 
`slice(df, 1:10)`|` df.iloc[:9]` | SQL
`filter(df, col1 == 1, col2 == 1)`|` df.query('col1 == 1 & col2 == 1')` | SQL
`df[df$col1 == 1 & df$col2 == 1,]`|` df[( df.col1 == 1) & (df.col2 == 1)]` | SQL
`select(df, col1, col2)`|` df[['col1', 'col2']]` | SQL
`select(df, col1:col3)`|` df.loc[:, 'col1':'col3']` | SQL
`select(df, -(col1:col3))`|` df.drop(cols_to_drop, axis=1) but see [1]` | SQL
`distinct(select(df, col1))`|` df[['col1']].drop_duplicates()` | SQL
`distinct(select(df, col1, col2))`|`  df[['col1', 'col2']].drop_duplicates()` | SQL
`sample_n(df, 10)`|` df.sample(n=10)` | SQL
`sample_frac(df, 0.01)`|` df.sample(frac=0.01)` | SQL
`subset(df, x<ab)`|` df.sample(frac=0.01)` | SQL

```py
# apply data type to column
active['team.name'] = active['team.name'].astype('category')
```

## Pandas 

### Overview

```python
# Define Dataframe with columns and indices using dictionary of equal length
df = DataFrame(data, columns=['year', 'state', 'pop'], index=['a', 'b', 'd'])  

df['state'] = df.state          # select a column
df.ix['three']                  # select row 
frame2['pop'] = np.arange(5.)   # assign values to column
del df['pop']                   # delete a column
df.T                            # transpose df
df.values # returns data in a 2D ndarray

pop = {'Nevada': {2001: 2.4, 2002: 2.9}, 'Ohio': {2000: 1.5, 2002: 3.6}} # Create nested dict as datasource
df2 = DataFrame(pop)                 # uses years as indices, and assigns values if present

df2.ix[['1999', '2001', '2002'],  ['Nevada', 'California']] # reindex both axis, using NAs for 1st row and 2nd column
df2[:2]                              # only use first two rows
df2[df2['Nevada'] > 5]               # Dataframe where value in nevada column is bigger than 5
df2 < 5                              # shows df with True for values smaller than 5, otherwise False
df.sort_index(by=['Nevada', 'Ohio']) # Sort by columns, first Nevada than Ohio
```

### Overview
```python
# Apply function to each row
f = lambda x: x.max()-x.min()
df.apply(f)                      

# Element-wise application of a function
format = lambda x: '%.2f' % x
df.applymap(format)              

# Read Time series data
data = pd.read_csv('http://hilpisch.com/eurusd.csv', index_col=0, parse_dates=True)

# Get data overview
data.info()

# Drop NA
df.dropna(inplace=True)

df[['path', 'Pred']].iloc[-30:].plot(figsize=(15,6))

# Random walk
S0 = 100
T = 1.0
sigma = 0.2
M = 200
dt = T / M
S = S0 *np.exp((-0.5 * sigma ** 2) * dt + sigma * rn * dt ** 0.5).cumsum(axis=0)

# cummulated sum
data[['Returns','Strategy']].cumsum().apply(np.exp).plot(figsize=(10,6))

# Count amount of values
.value_counts()

# Filter Data row-wise
data = data[data.index > '2014-1-1']

# Calculate Rolling Average
data['SMA2'] = data['Mid'].rolling(60).mean()

# Add if column
data['Position'] = np.where(data['SMA1'] > data['SMA2'], 1,-1)

# plot two different scales in same plot
data[['Mid','SMA1','SMA2','Position']].plot(figsize=(10,6),secondary_y='Position')
```


### Basic timeseries analysis

```python
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
dateparse = lambda x: datetime.strptime(x, ""%Y-%m-%d")
df = pd.read_csv("query_result.csv", parse_dates=["p_daystring_utc"], date_parser=dateparse)
df.columns=["country","date","count"]
plt.plot(df)

```


### Basic timeseries analysis

```python
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
%matplotlib inline
    
df = pd.read_json("/Users/nkreiling/Downloads/failures.json")
df['date'] = pd.to_datetime(df['date'],format="%Y-%m-%d")
df.set_index('date', inplace=True)
df.dropna(inplace=True)
pd.value_counts(df['date']).sort_index().plot.bar(figsize=(17,6))
```



import seaborn as sns